package com.example.demo;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	static String [] array1 = new String[]{"juan", "camilo", "rodriguez"};
	static String [] array2 = new String[]{"juan", "camilo", "rodriguez"};

	static boolean isValidName = true;
	static boolean fstate = false;

	static String nullString = null;
	static String notnullString = "juan";

	@Test
	void contextLoads() {
	}

	@Test
	void validateArray1equalsToArray2(){
		Assertions.assertArrayEquals(array1, array2);
	}

	@Test
	void validIfNameTrue(){
		Assertions.assertTrue(isValidName);
	}

	@Test
	void validIfFstate(){
		Assertions.assertFalse(fstate);
	}

	@Test
	void validIfStringIsNull(){
		Assertions.assertNotNull(notnullString, "juan");
	}

	@Test
	void validaIfArraysAreTheSame(){
		Assertions.assertNotSame(array1, array2);
	}



}
